const express = require("express");
const bodyParser = require("body-parser");
const morgan = require("morgan");
const database = require("./config/database");
const routes = require("./api/routes/index");
const cookieParser = require("cookie-parser");
const app = express();

require("dotenv").config();

app.use(morgan("tiny"));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: false }));
app.use("/api", routes);
app.listen(process.env.PORT, () => {
	console.log(
		`Server Started on Url ${process.env.URL}:${process.env.PORT}`
	);
});
database().connect();

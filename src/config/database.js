const mongoose = require("mongoose");

const database = () => {
	const connect = () => {
		return mongoose
			.connect(process.env.DB_URL, {
				useNewUrlParser: true,
			})
			.then(() => {
				console.log("Connected to database");
			})
			.catch((err) => console.log(err));
	};
	return { connect };
};
module.exports = database;
